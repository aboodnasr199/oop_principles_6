package class_vs_object;

public class House_Practice {
    public static void main(String[] args) {
        House house1 = new House();

        System.out.println(house1); //class_vs_object.House@1540e19d = location

        house1.price = 300000;
        System.out.println(house1);

        house1.color = "blue";
        house1.year = 2020;
        System.out.println(house1);


        System.out.println("\n------more objects----\n");
        House house2 = new House();
        House house3 = new House();
        House house4 = new House();
        House house5 = new House();

        System.out.println(house2);
        System.out.println(house3);
        System.out.println(house4);
        System.out.println(house5);




    }
}
