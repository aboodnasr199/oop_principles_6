package class_vs_object;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Arrays;

public class House_Exercise {
    public static void main(String[] args) {
        /*
        -Constructor is a special method use to instantiate an object of a class
        -it always starts with the same name
        -Java always provides a default constructor when creating a class (hidden)

        Differences of methods and constructor:
        1. Method can be static while constructor cannot
        2. Method can be void or return while constructor cannot

        Similiarties
        1. Both can take arguments
        2. both can be overloaded
         */

        House house1 = new House("Blue", 200000, 2020, "Chicago", true);
        House house2 = new House("Yellow", 250000, 2021, "Miami", true);
        House house3 = new House("Green", 300000, 2022, "Chicago", false);
        House house4 = new House("Brown", 350000, 2023, "Miami", false);
        House house5 = new House("Blue", 400000, 2023, "Chicago", true);
        // 5 houses - 5 objects
        // dataType[] varName =

        House[] houses = {house1, house2, house3, house4, house5};

        ArrayList<House> houseList = new ArrayList<>(Arrays.asList(house1, house2, house3, house4, house5));


        System.out.println("\n------Looping house array-----\n");
        for (House house : houses) {
            System.out.println(house);

        }


        System.out.println("\n------Looping a house arrayList-----\n");
        for (House house : houseList) {
            System.out.println(house);

        }

        System.out.println("\n------For each Method arrayList-----\n");
        houseList.forEach(System.out::println);

        System.out.println("\n-----Print color for each house-----\n");
        for (House house : houses) {
            System.out.println(house.color);

        }

        /*
        count how many houses in chicago
         */
        System.out.println("\n-----Count house in chicago-------\n");
        int count = 0;

        for (House element : houses) {
            if (element.address.equals("Chicago")){
                count++;
            }

        }
        System.out.println(count);

        System.out.println("\n-----Count house in chicago and has garden-------\n");
        count = 0;
        System.out.println(houseList.stream().filter(element -> element.hasGarden && element.address.equalsIgnoreCase("Chicago")).count());

    }
}
